#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "game.h"
#include "config.h"

int main (int argc, char **argv){

  GameConfig *gc = game_config_new_from_cli(argc, argv);
  Game *game = game_new();

  game_parse_board(game, gc);

  int genCounter = 0;

  if(!gc->silent){
    printf("%d \n \n", genCounter);
    game_print_board(game);
  }

  //Start clock
  clock_t start = clock();

  while(genCounter < gc->generations){
    genCounter++;
    if(game_tick(game)==1){
      printf("The tick could not happen correctly");
      exit(1);
    }
    else{
      if(gc->debug || (genCounter == gc->generations && gc->silent == 0)){
        printf("%d \n \n", genCounter);
        game_print_board(game);
        printf("\n");
      }
    }
  }
  game_free(game);
  game_config_free(gc);

  //end clock
  clock_t end = clock();
  float seconds = (float)(end - start) / CLOCKS_PER_SEC;

  printf("---------> Run time: %f seconds \n \n", seconds);
}
