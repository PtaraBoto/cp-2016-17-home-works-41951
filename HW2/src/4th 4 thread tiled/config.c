#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "config.h"

void game_config_free(GameConfig *config){
  free(config);
}

size_t game_config_get_generations(GameConfig *config){
  return config->generations;
}

GameConfig *game_config_new_from_cli(int argc, char *argv[]){

  GameConfig *gc = (GameConfig *) malloc (sizeof (GameConfig));

  //default values
  gc->debug = 0;
  gc->silent = 0;
  gc->generations = 21;

  int c;

  while ((c = getopt (argc, argv, "dn:s")) != -1)
    switch (c)
      {
      case 'd':
        gc->debug = 1;
        break;
      case 'n':
        if(optarg != NULL)
          gc->generations = atoi(optarg);
        break;
      case 's':
        gc->silent = 1;
        break;
      }

    if(gc->debug == 1 && gc->silent == 1){
      printf ("Cannot use -d and -s simultaneously! \n");
      exit(1);
    }

    gc->input_file = fopen(argv[argc - 1], "r");

    return gc;
}
