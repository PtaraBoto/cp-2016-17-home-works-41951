#include <stdlib.h>
#include <stdio.h>
#include <cilk/cilk.h>

#include "game.h"

void game_free(Game *game){
  free(game);
}

int game_cell_is_alive(Game *game, size_t row, size_t col){

  char pos = game->board[coordenatesToIndex(game, row, col)];

  if(pos == '.')
    return 0;
  else
    return 1;
}

int game_cell_is_dead(Game *game, size_t row, size_t col){

  char pos = game->board[coordenatesToIndex(game, row, col)];

  if(pos == '#')
    return 1;
  else
    return 0;
}

Game *game_new(void){
    Game *g = (Game *) malloc (sizeof (Game));
    return g;
}

int game_parse_board(Game *game, GameConfig *config){

  int rows;
  int collums;
  char buffer [30];

  //Obtain initial values from file
  fgets(buffer, sizeof buffer, config->input_file);
  sscanf(buffer, "Rows:%d",&rows);

  fgets(buffer, sizeof buffer, config->input_file);
  sscanf(buffer, "Cols:%d",&collums);

  game->cols = collums;
  game->rows = rows;

  //Assemble board
  game->board = (char *) malloc(sizeof(char)* (collums*rows));

  char boardbuffer [collums+1];
  int counterRows = 0;

  while(counterRows < rows){

    int counterCollums = 0;
    fgets(boardbuffer, collums + 1, config->input_file);

    while(counterCollums < collums){

      game->board[coordenatesToIndex(game, counterRows, counterCollums)] = boardbuffer[counterCollums];
      counterCollums++;
    }
    fgets(boardbuffer, collums + 1, config->input_file); // To discard \n
    counterRows++;
  }
  return 0;
}

void game_print_board(Game *game){

  int charToPrint = 0;
  int rowCounter = 0;

  while(rowCounter < game->rows){

    int collumCounter = 0;
    while(collumCounter < game->cols){

      printf("%c",game->board[charToPrint]);
      collumCounter++;
      charToPrint++;
    }
    printf("\n");
    rowCounter++;
  }
  printf("\n");
}

void game_cell_set_alive(Game *game, size_t row, size_t col){
  game->board[coordenatesToIndex(game, row, col)] = '#';
}

void game_cell_set_dead(Game *game, size_t row, size_t col){
  game->board[coordenatesToIndex(game, row, col)] = '.';
}

int game_tick(Game *game){

  int sizeOfString = game->cols * game->rows;
  static char *newBoard;// = (char *) malloc(sizeof(char) * (sizeOfString));

  if(newBoard == NULL)
    newBoard = (char *) malloc(sizeof(char) * (sizeOfString));

  int counter;

  cilk_for(counter = 0; counter <= sizeOfString; counter++){
    newBoard[counter] = processPosition(game, counter);
  }

  // while(counter < sizeOfString){
  //   int rowPos = (counter/game->cols);
  //   int collumPos = (counter%game->cols);
  //
  //   if(processPosition(game, rowPos, collumPos))
  //     newBoard[counter] = '#';
  //   else
  //     newBoard[counter] = '.';
  //
  //   counter++;
  // }

  swap(&game->board, &newBoard);
  return 0;
}

char processPosition(Game *game, int pos){

  int rowPos = (pos/game->cols);
  int collumPos = (pos%game->cols);

  int numberOfNeighborsAlive = 0;

  // int rowCounter = -1;
  // while(rowCounter < 2){
  //
  //   int collumCounter = -1;
  //   while(collumCounter < 2){
  //
  //     if(!(rowCounter == 0 && collumCounter == 0))
  //       if(processNeighbor(game, rowPos + rowCounter, collumPos + collumCounter))
  //         numberOfNeighborsAlive++;
  //
  //     collumCounter++;
  //   }
  //
  //   if(numberOfNeighborsAlive > 3)
  //     break;
  //   rowCounter++;
  // }

  //Check north
  if(processNeighbor(game, rowPos - 1, collumPos))
    numberOfNeighborsAlive++;
  //Check east
  if(processNeighbor(game, rowPos, collumPos + 1))
    numberOfNeighborsAlive++;
  //Check south
  if(processNeighbor(game, rowPos + 1, collumPos))
    numberOfNeighborsAlive++;
  //Check weast
  if(processNeighbor(game, rowPos, collumPos - 1))
    numberOfNeighborsAlive++;
  //Check northeast
  if(processNeighbor(game, rowPos - 1, collumPos + 1))
    numberOfNeighborsAlive++;
  //Check Southeast
  if(processNeighbor(game, rowPos + 1, collumPos + 1))
    numberOfNeighborsAlive++;
  //Check Southweast
  if(processNeighbor(game, rowPos + 1, collumPos - 1))
    numberOfNeighborsAlive++;
  //Check NorthWeast
  if(processNeighbor(game, rowPos - 1, collumPos - 1))
    numberOfNeighborsAlive++;

  //printf("live:%d \n", numberOfNeighborsAlive);

  //Apply rules
  if(numberOfNeighborsAlive < 2)
    return '.';
  if(numberOfNeighborsAlive > 3)
    return '.';
  if(numberOfNeighborsAlive == 3)
    return '#';
  else{
    if(game_cell_is_alive(game, rowPos, collumPos))
      return '#';
    else
      return '.';
  }
}

int processNeighbor(Game *game, int rowPos, int collumPos){
  if(rowPos == -1)
    rowPos = game->rows - 1;
  else if(rowPos == game->rows)
    rowPos = 0;

  if(collumPos == -1)
    collumPos = game->cols - 1;
  else if(collumPos == game->cols)
    collumPos = 0;

  return game_cell_is_alive(game, rowPos, collumPos);
}

int coordenatesToIndex(Game *game, int row, int col){
  return (row*game->cols) + col;
}

void swap(char **oldB, char **newB){
  char *temp = *oldB;
  *oldB = *newB;
  *newB = temp;
}
